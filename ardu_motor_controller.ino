
/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: ardu_motor_controller.ino
        Version:   v0.0
        Date:      07-11-2019
    ============================================================================
*/

/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/


/*
** =============================================================================
**                        DEFINES AND MACROS
** =============================================================================
*/

#include <SPI.h>
#include <NRFLite.h>
#include <Wire.h>
#include <Adafruit_INA219.h>
#include "DHT.h"

#define CUBE_GET '<'
#define CUBE_SET '>'
#define MESSAGE_BYTES 10

#define DHTTYPE DHT11   
#define MOTOR1_DHTPIN  A0 /* Motor1 tmperatur sensor */ 
#define MOTOR2_DHTPIN  A1 /* Motor2 tmperatur sensor */ 

#define MOTOR1_EN      4  /* Motor1 enable control terminal */
#define MOTOR1_RPWM    5  /* Motor1 Right speed connect to PWM */
#define MOTOR1_LPWM    3  /* Motor1 Left speed connect to PWM */
#define MOTOR2_RPWM    9  /* Motor2 Right speed connect to PWM */
#define MOTOR2_LPWM    6  /* Motor2 Left speed connect to PWM */


/*
** =============================================================================
**                       Function Prototypes
** =============================================================================
*/

void setPwmFrequency(int pin, int divisor);
void serialRead(void);
void motorSaftyStop(void);
void motorsStart(void);
int procesMsg(void);

/*
** =============================================================================
**                        Global Variables
** =============================================================================
*/

char payload[MESSAGE_BYTES];

static int16_t motorSpeed = 255;
static uint8_t motorSpeedMax = 255;

/*********************************************************/
const static uint8_t RADIO_ID = 0;
const static uint8_t DESTINATION_RADIO_ID = 1;
const static uint8_t PIN_RADIO_CE = 7;
const static uint8_t PIN_RADIO_CSN = 8;
const static uint8_t PIN_RADIO_IRQ = 2;

enum RadioPacketType
{
  Heartbeat,
  BeginGetData,
  EndGetData,
  ReceiverData
};

struct RadioPacket
{
  RadioPacketType PacketType;
  uint8_t FromRadioId;
  uint8_t Direction;
  uint8_t Speed;
  uint8_t Motor1Tmp;
  uint8_t Motor2Tmp;
  uint32_t Uptime;
};

DHT dht_m1(MOTOR1_DHTPIN, DHTTYPE);
DHT dht_m2(MOTOR2_DHTPIN, DHTTYPE);

NRFLite _radio;
volatile uint8_t _dataWasReceived;
uint32_t _lastHeartbeatSendTime;


/*********************************************************/


/*
** =============================================================================
**                        FUNCTION DECLARATION
** =============================================================================
*/

/*==============================================================================
** Function...: setup
** Return.....: void
** Description: Initialization function
** Created....: 26.06.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setup() {

  Serial.begin( 115200 );

  if (!_radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN))
  {
    Serial.println("Cannot communicate with radio");
    while (1); // Wait here forever.
  }

  dht_m1.begin();
  dht_m2.begin();

  attachInterrupt(digitalPinToInterrupt(PIN_RADIO_IRQ), radioInterrupt, FALLING);

  pinMode( MOTOR1_EN, OUTPUT );
  pinMode( MOTOR1_RPWM, OUTPUT );
  pinMode( MOTOR1_LPWM, OUTPUT );
  pinMode( MOTOR2_RPWM, OUTPUT );
  pinMode( MOTOR2_LPWM, OUTPUT );

  digitalWrite(MOTOR1_EN, HIGH);

  Serial.println( "Prime Motor Driver Ready" );
}


/*==============================================================================
** Function...: loop
** Return.....: void
** Description: Main function 
** Created....: 26.06.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void loop() {

  serialRead();

  if (_dataWasReceived)
  {
    _dataWasReceived = false;
    while (_radio.hasDataISR())
    {
      Serial.println("Got data");
      RadioPacket radioData;
      _radio.readData(&radioData);
      handleRadioIn(&radioData);
    }
  }

  // Send a heartbeat once every 2-seconds.
  if (millis() - _lastHeartbeatSendTime > 1998)
  {
    _lastHeartbeatSendTime = millis();
    
    RadioPacket  heartBeatData;
    heartBeatData.PacketType = Heartbeat;
    heartBeatData.FromRadioId = RADIO_ID;
    heartBeatData.Uptime =  _lastHeartbeatSendTime / 1000;
    getDhtData( &heartBeatData );
    sendRadioData(heartBeatData);
  }
}

void getDhtData( RadioPacket  *radioData )
{
  float t1 = dht_m1.readTemperature();
  float t2 = dht_m2.readTemperature();

  radioData->Motor1Tmp =  t1;
  radioData->Motor2Tmp =  t2;

  if(t1 > 40 || t2 > 40)
  {
    motorSaftyStop();
  }

  // Check if any reads failed and exit early (to try again).
  if ( isnan(t1) ) {
    Serial.println("Failed to read from DHT sensor!");
  }

}


void sendRadioData( RadioPacket  radioData )
{
  if (_radio.send(DESTINATION_RADIO_ID, &radioData, sizeof(radioData)))
  {
    Serial.println("...Success");
  }
  else
  {
    Serial.println("...Failed");
  }

  _radio.startRx();
}

void handleRadioIn( RadioPacket  *radioData )
{
  if ( radioData->Direction == 0 )
  {
    Serial.println("...Failed");
  }
  else if ( radioData->Direction == 1 )
  {
    motorsStart();
    analogWrite(MOTOR1_RPWM, 0);
    analogWrite(MOTOR1_LPWM, motorSpeed);
    analogWrite(MOTOR2_RPWM, 0);
    analogWrite(MOTOR2_LPWM, motorSpeed);
    Serial.print("down");
  }
  else if ( radioData->Direction == 2 )
  {
    motorsStart();
    analogWrite(MOTOR1_RPWM, motorSpeed);
    analogWrite(MOTOR1_LPWM, 0);
    analogWrite(MOTOR2_RPWM, motorSpeed);
    analogWrite(MOTOR2_LPWM, 0);
    
    Serial.print("up ");
  }
  else if ( radioData->Direction == 3 )
  {
    motorsStart();
    analogWrite(MOTOR1_RPWM, 0);
    analogWrite(MOTOR1_LPWM, motorSpeed);
    analogWrite(MOTOR2_RPWM, motorSpeed);
    analogWrite(MOTOR2_LPWM, 0);
    Serial.print("left ");
  }
  else if ( radioData->Direction == 4 )
  {
    motorsStart();
    analogWrite(MOTOR1_RPWM, motorSpeed);
    analogWrite(MOTOR1_LPWM, 0);
    analogWrite(MOTOR2_RPWM, 0);
    analogWrite(MOTOR2_LPWM, motorSpeed);
    Serial.print("right ");
  }
  else if ( radioData->Direction == 5 )
  {
    Serial.print("stop ");
    motorSaftyStop();
  }

  Serial.println(radioData->Speed);

  motorSpeed = radioData->Speed;

}

void serialRead(void)
{
  int lf = 10;
  if (Serial.available() > 0) 
  {
    Serial.readBytesUntil(lf, payload, 64);
    procesMsg();
    
    
    for (int i = 0; i < MESSAGE_BYTES; i++)
    {
      Serial.println(payload[i]);
    }
    Serial.println();
    
  }
}

int procesMsg(void)
{
  char cube_request   = payload[0];
  char cmd_1 = payload[1]; 
  char cmd_2 = payload[2];
  //int  cmd_2   = atoi(payload + 3);
  char s[50];

  if(cube_request==CUBE_GET)
  {
    // COM_PWR_CTL status REQ
    if(cmd_1 == '1')
    {
      sprintf(s,"GET REQ ACK OK");
    }
    else
    {
      sprintf(s,"GET REQ ACK FAILED");
    }
    Serial.println(s);
  }
  else if(cube_request==CUBE_SET)
  {
    if(cmd_1=='1')
    {
      sprintf(s,"SET REQ ACK OK");
    }
    else
    {
      sprintf(s,"SET REQ ACK FAILED");
    }
    Serial.println(s);
  }
  else
  {
    sprintf(s,"FAIL: %d %d %d\n", cube_request, cmd_1, cmd_2);
    Serial.println(s);
  }

  return 0;
}

void motorSaftyStop(void)
{
    digitalWrite(MOTOR1_EN, LOW);
    analogWrite(MOTOR1_RPWM, 0);
    analogWrite(MOTOR1_LPWM, 0);
}

void motorsStart(void)
{
    digitalWrite(MOTOR1_EN, HIGH);
    analogWrite(MOTOR1_RPWM, 0);
    analogWrite(MOTOR1_LPWM, 0);
}

void radioInterrupt()
{
  // Ask the radio what caused the interrupt.  This also resets the IRQ pin on the
  // radio so a new interrupt can be triggered.

  uint8_t txOk, txFail, rxReady;
  _radio.whatHappened(txOk, txFail, rxReady);

  // txOk = the radio successfully transmitted data.
  // txFail = the radio failed to transmit data.
  // rxReady = the radio received data.

  if (rxReady)
  {
    _dataWasReceived = true;
  }
}
